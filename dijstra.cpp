
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <list>
#include <queue>
#include <stack>
#include <iomanip>


using namespace std;

struct campo
{
	public:
		string estacion_1;
		string estacion_2;
		double distancia;
	public:
		campo(string estacion_1,string estacion_2);
};

struct ant
{
	public:
		string est;
		string anterior;
	public:
		ant(string est, string anterior);
};

campo::campo(string estacion_1,string estacion_2)
{
	this->estacion_1=estacion_1;
	this->estacion_2=estacion_2;
	this->distancia=1000000;
}
campo *crear_campo(string estacion_1,string estacion_2);
campo *crear_campo(string estacion_1,string estacion_2)
{
	campo *n=new campo(estacion_1,estacion_2);
	return n;
}

ant::ant(string est, string anterior)
{
	this->est=est;
	this->anterior=anterior;
}

void crear_indice(string ruta,list <string> &lista_de_estaciones);
void crear_matriz(list <string> &lista_de_estaciones,list <campo> &matriz);
void llenar_matriz(string ruta,list <string> &lista_de_estaciones,list <campo> &matriz);
void Dijstra(list <string> &estaciones, list <campo> &matriz);
double distancia(string estacion_1,string estacion_2,list <campo> &matriz);

void crear_indice(string ruta,list <string> &lista_de_estaciones)
{
	string compare;
	ifstream entrada (ruta.c_str(), ios_base::in);
	if (entrada.good()){
		do{
			getline(entrada,compare);
			if (compare=="&"){
				getline(entrada,compare);
				lista_de_estaciones.push_back(compare);
			}
		}while(!entrada.eof());
	}else{
		cout<<"Error"<<endl;
	}
	entrada.close();
}

void crear_matriz(list <string> &lista_de_estaciones,list <campo> &matriz)
{
	campo *aux;
	for (list <string>::iterator it=lista_de_estaciones.begin(); it!=lista_de_estaciones.end(); ++it){
		for (list <string>::iterator jt=lista_de_estaciones.begin(); jt!=lista_de_estaciones.end(); ++jt){
			aux=crear_campo(*it,*jt);
			if (*it==*jt)
				aux->distancia=0;
			matriz.push_back(*aux);
		}
	}
}

void llenar_matriz(string ruta,list <string> &lista_de_estaciones,list <campo> &matriz)
{
	int n;
	double dist;
	bool op;
	queue <string> cola;
	string bufer,aux;
	ifstream entrada (ruta.c_str(), ios_base::in);
	list <string>::iterator iter=lista_de_estaciones.begin();
	if (entrada.good()){
		do{
			getline(entrada,bufer);
			if (bufer=="&"){
				getline(entrada,aux);
				if (aux==*iter){
					getline(entrada,bufer);
					n=atoi(bufer.c_str());
					for (int i = 0; i < n; ++i){
						list <string>::iterator iter2=lista_de_estaciones.begin();
						op=true;
						getline(entrada,bufer);
						while(iter2!=lista_de_estaciones.end()&&op){
							if (bufer==*iter2)
								op=false;
							else
								iter2++;
						}
						if (op){
							cout<<"El txt tiene errores en la adyacencia de "<<*iter<<" con "<<bufer<<"."<<endl;
							system("pause");
						}
						cola.push(bufer);
					}
					while(!cola.empty()){
						getline(entrada,bufer);
						dist=atof(bufer.c_str());
						list <campo>::iterator mat=matriz.begin();
						op=true;
						while(mat!=matriz.end()&&op){
							if(*iter==mat->estacion_1&&cola.front()==mat->estacion_2&&aux==mat->estacion_1){
								mat->distancia=dist;
								op=false;
							}else
								mat++;
						}cola.pop();
						if(op)
							cout<<"El txt tiene errores en "<<*iter<<" con "<<cola.front()<<"."<<endl;
					}
				}else{
					cout<<"el txt tiene errores en "<<*iter<<endl;
				}
				iter++;
			}
		}while(!entrada.eof());
	}else{
		cout<<"Error"<<endl;
	}
	entrada.close();
}

double distancia(string estacion_1,string estacion_2,list <campo> &matriz)
{
	double dist=1000000;
	list <campo>::iterator mat=matriz.begin();
	bool op=true;
	while(mat!=matriz.end()&&op){
		if(mat->estacion_1==estacion_1&&mat->estacion_2==estacion_2){
			dist=mat->distancia;
	   		op=false;
		}else
	   		mat++;
	}
	return dist;
}

void Dijstra(list <string> &estaciones, list <campo> &matriz)
{
	string inicio;
	string fin;
	stack <string> S;
	list <string> V_S;
	list <double> dist;
	list <ant> ante;
	list <string> ruta;
	bool op,bandera=false;
	list <string>::iterator iter2;
	string chaba="chabacano";
	system("cls");
	do{
		cout<<"En donde estas?"<<endl;
		fflush(stdin);
		getline(cin,inicio);
		op=true;
		iter2=estaciones.begin();
		while(iter2!=estaciones.end()&&op){
			if(*iter2==inicio)
				op=false;
			else
				iter2++;
		}if (op){
			cout<<"Ese nombre no se encuentra en la base de datos intentalo de nuevo."<<endl;
			system("pause");
		}
	}while(op);
	if (inicio==chaba){
				bandera=true;
		}
	do{
		cout<<"A donde quieres llegar?"<<endl;
		fflush(stdin);
		getline(cin,fin);
		op=true;
		iter2=estaciones.begin();
		while(iter2!=estaciones.end()&&op){
			if(*iter2==fin)
				op=false;
			else
				iter2++;
		}
		if (op){
			cout<<"Ese nombre no se encuentra en la base de datos, intentalo de nuevo."<<endl;
			system("pause");
		}
	}while(op);
	if(bandera){
		string cha;
		cha=inicio;
		inicio=fin;
		fin=cha;
	}

	S.push(inicio);
	for (list <string>::iterator iter=estaciones.begin(); iter!=estaciones.end(); iter++){
		if (*iter!=inicio){
			V_S.push_back(*iter);
		}
	}
	for (list <string>::iterator iter=V_S.begin(); iter!=V_S.end(); ++iter){
		ant *aux1 = new ant(*iter, inicio);
		ante.push_back(*aux1); // P{V}
		dist.push_back(distancia(inicio,*iter,matriz));
	}
	cout<<"Procesando...";
	while(!V_S.empty()){
		double dist_min=1000000;
		list <string>:: iterator it;
		list <double>::iterator dv;
		list <string>:: iterator itr;
		list <double>::iterator du;
		dv=dist.begin();
		for (it = V_S.begin(); it!=V_S.end(); ++it){
			if(*dv < dist_min){
				dist_min=*dv;
				itr=it;
				du=dv;
			}
			dv++;
		}
		S.push(*itr);
		itr=V_S.erase(itr);
		double aux=*du;
		du=dist.erase(du);
		dv=dist.begin();
		list <ant>::iterator pv=ante.begin();
		for(it=V_S.begin(); it!=V_S.end(); ++it){
			double distanci=distancia(*it,S.top(),matriz);
			if ( (aux+distanci) < *dv){
				*dv=(aux+distanci);
				bool opc = true;
				pv=ante.begin();
				while(opc){
					if (pv->est == *it ){
						pv->anterior=S.top();
						opc=false;
					}
					else
						pv++;
				}
			}
			dv++;
		}
	}
	list <ant>::iterator indice=ante.begin();
	string final=fin;
	bool flag= true;
	while(flag){
		if (indice->est == final){
			ruta.push_front(indice->anterior);
			final=indice->anterior;
			if (final==inicio){
				flag=false;
			}
		}
		else
			indice++;
	}
	system("cls");
	if (bandera){
		cout<<"La ruta mas corta de "<<fin<<" a "<<inicio<<" es:"<<endl<<endl;
		int x=0;
		for(list <string>::iterator it=ruta.end();it!=ruta.begin();--it){
			x++;
			if(x==1){
				cout<<setw(3)<<x<<". "<<fin<<endl;
				}else{
					cout<<setw(3)<<x<<". "<<*it<<endl;
				}
		}
		cout<<setw(3)<<x+1<<". "<<inicio<<endl<<endl;
	}
	else{
		cout<<"La ruta mas corta de "<<inicio<<" a "<<fin<<" es:"<<endl<<endl;
		int x=0;
		for (list <string>::iterator it = ruta.begin(); it!=ruta.end(); ++it){
			x++;
			cout<<setw(3)<<x<<". "<<*it<<endl;
			}
			cout<<setw(3)<<x+1<<". "<<fin<<endl<<endl;
		}
	system("pause");
}

int main(int argc, char const *argv[])
{
	list <campo> matriz;
	list <string> estaciones;
	string ruta;
	cout<<"Introduce la ruta del txt con los datos del metro:"<<endl;
	fflush(stdin);
	getline(cin,ruta);
	ifstream prueba (ruta.c_str(), ios_base::in);
	if (prueba.good())
	{
		crear_indice(ruta,estaciones);
		crear_matriz(estaciones,matriz);
		llenar_matriz(ruta,estaciones,matriz);
		cout<<"Archivo cargado exitosamente."<<endl;
		system("pause");
		Dijstra(estaciones,matriz);
		int opc=0;
		do{
			bool continuar;
			int cont=0;
			do
			{
	      		system ("cls");
	      		cout<<" 1. Calcular otra ruta."<<endl;
	      		cout<<" 2. Salir."<<endl;
	      		cout<<endl;
	      		continuar=false;
	      		cin.clear();
	      		if(cont > 0) cin.ignore(1024, '\n');
	      		cin>>opc;
	      		cont++;
			    if(cin.fail() && cin.rdstate())
			    {
			    	cout << "Opcion invalida." << endl;
			        system("pause");
			        continuar = true;
			    }
			} while (continuar);
			switch (opc)
			{
				case 1:
					Dijstra(estaciones,matriz);
				break;
				case 2:
				break;
				default:
					cout<<"Opcion invalida."<<endl;
					system("pause");
				break;
			}
		}while(opc!=2);
	}
	else{
		cout<<"Error, no se pudo leer la ruta."<<endl;
		system("pause");
	}
	prueba.close();
	return 0;
}
